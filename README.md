This repository groups a set of tools to easily use OAR.


# Requirements

Obviously, OAR needs to be installed on the system to be able to make use of these commands.

Here are the requirements for each script:

-   `oargen` requires python 2+ or 3+. It uses the `oarsub` command from OAR.
-   `oarstats` requires python 2+ or 3+ and the *pyyaml* package. It uses the `oarstat` command from OAR.
-   `batchoar` requires python 3+ and the *pyyaml* package. It uses the `oarsub` command from OAR.


# Usage


## OARGEN

The basic usage of `oargen` is simply `python oargen.py -r command` where *command* is the job
to execute on the cluster.

Several options can be used:

-   --run: run the job on the cluster.
-   --time *TIME*: reservation walltime of your job. Accepted formats: *h*, *h:m* or *h:m:s*. Defaults to 10 hours.
-   --core *NB\_CORES*: number of cores required by your job. Defaults to 1.
-   --interative: launch job in interative mode instead of passive.
-   --gpu: request GPUs.
-   --host *host1* [ *host2* ... *hostn* ]: name of the hosts allowed to be used on the cluster.
-   --ignore-host *host1* [ *host2* ... *hostn* ]: name of the hosts forbidden to be used on the cluster.
-   --anterior *ANTERIOR\_ID*: job will only be launched once the job with the specified ID is finished.
-   --checkpoint *delay*: enable the checkpoint signal with the given delay (in seconds).
-   --name *name*: name of the job.
-   --directory *directory*: directory in which will be stored the logs of the standard output and input.


## OARSTATS

The basic usage of `oarstats` is simply `python oarstats.py`.
It will print some information about the number of jobs, cores and gpus used by
each user. It will also take into account the different queues.

One option can be used:

-   --show-hosts: Adds an extra line for each user indicating which hosts are being used.

